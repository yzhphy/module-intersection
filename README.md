# ModuleIntersection

ModuleIntersection is a Mathematica interface for finding the IBP with restricted indices.


 

  
  
### To install ModuleIntersection


1. Install *Singular* from [https://www.singular.uni-kl.de](https://www.singular.uni-kl.de) .
2. 		
	```
	git clone git@bitbucket.org:yzhphy/module-intersection.git
	```
	
3. Create an empty directory on your local computer for temporary files. 


