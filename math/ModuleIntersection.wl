(* ::Package:: *)

LT[exp_,var_,order_]:=MonomialList[exp,var,order][[1]];


ListIntersect[set1_,set2_]:=If[MemberQ[set2,#],#,Nothing]&/@set1;   (* Intersection of list, which keeps the ordering in set1 *)


PolynomialDeg[poly_,var_]:=If[poly===0,-\[Infinity],Total[CoefficientRules[poly,var,DegreeReverseLexicographic][[1,1]]]];


monomialAnsatz[var1_,deg_]:=(Times@@MapThread[#1^#2&,{var1,Exponent[#,var1]}])&/@MonomialList[(1+Total[var1])^deg//Expand,var1,DegreeReverseLexicographic]


SimplificationRules::usage="";
ScriptFile::usage="";
ScriptOnly::usage="";
OutputFile::usage="";
HighestDegree::usage="";
zeroDeg::usage="";
WorkingDegree::usage="";
degBound::usage="";
MaxCut::usage="";
primeList::usage="";
TruncationFirst::usage="";
VariableOrder::usage="";
Cut::usage="";


pivots[matrix_]:=Module[{ARLonglist},
	ARLonglist=GatherBy[matrix//ArrayRules,#[[1,1]]&];
	Return[#[[1,1,2]]&/@(ARLonglist[[;;-2]])];
];


positivity[list_]:=If[Union[#>0&/@list]==Head[list][True],True,False];


(* ::Section:: *)
(*Sector Ordering*)


Sector[int_]:=Table[If[int[[j]]>0,1,0],{j,1,Length[int]}]
SectorIndex[int_]:=Table[If[int[[j]]>0,j,Nothing],{j,1,Length[int]}]
Index2Sector[propIndex_]:=Exponent[Times@@(z/@propIndex),var];


SectorCutQ[sec_,cut_]:=And@@(#>=0&/@(sec-cut));
SectorNumber[sec_]:=FromDigits[sec//Reverse,2];

SectorWeightMatrix1[sec_]:=Module[{propIndex,ISPIndex,matrix,i},
	propIndex=Position[sec,1]//Flatten;
	ISPIndex=Position[sec,0]//Flatten//Reverse; (* !!! *)
	matrix=SparseArray[{},{SDim,SDim}]//Normal;
	matrix[[1]]=Table[1,{j,1,SDim}];
	For[i=1,i<=Min[Length[propIndex],SDim-1],i++,
		matrix[[i+1,propIndex[[i]]]]=1;
	];
	For[i=1,i<=Length[ISPIndex]-1,i++,
		matrix[[Length[propIndex]+1+i,ISPIndex[[i]]]]=1;
	];
	Return[matrix];
]; (* DegreeLexicographic ordering for both the double propagator and ISPs *)


IntegralWeight[int_]:=SectorWeightMatrix1[int//Sector].IntegralAbsDegree[int];

IntegralCut[cut_]:=G[x__]:>If[SectorCutQ[Sector[G[x]],cut],G[x],0];
IntegralDegree[int_]:=(int/.G->List)-Sector[int];
IntegralAbsDegree[int_]:=Abs/@((int/.G->List)-Sector[int]);
IntegralSectorHeight[int_]:=Count[int/.G->List,u_/;u>0];
IntegralSectorOrder[int_]:=SectorWeightMatrix1[Sector[int]].IntegralAbsDegree[int];
IntegralReal[indices_]:=Dispatch[Table[m[i]->indices[[i]],{i,1,SDim}]];






IntegralOrdering[int_]:=Join[{IntegralSectorHeight[int],SectorNumber[int//Sector]},IntegralWeight[int]];


IntegralList[IBP_]:=Select[Variables[IBP],Head[#]==G&];

IBPWeight[IBP_]:=Max[Total[IntegralAbsDegree[#]]&/@IntegralList[IBP]];


CollectG[exp_]:=Coefficient[exp,Select[Variables[exp],Head[#]==G&]].Select[Variables[exp],Head[#]==G&];


(* ::Section:: *)
(*Baikov Representation*)


ScalarTangentVector[rowIndex1_,rowIndex2_]:=Module[{position,vector},
	position=Position[Global`ScalarVar,#][[1,1]]&/@(Global`BaikovMatrix[[rowIndex2]]);
	vector=SparseArray[Table[position[[i]]->If[i==rowIndex2,2,1]Global`BaikovMatrix[[rowIndex1,i]],{i,1,Length[position]}],SDim];
	Return[vector//Normal];
];


Prepare[]:=Module[{Formula,MatrixA,VectorB,ScalarC,BList,AList,Vectorb},
	ClearAll[m];
	n=Length[ExternalMomenta]+1;
	L=Length[LoopMomenta];
	
	zeroLoopMomenta=(#->0)&/@LoopMomenta;
	Momenta=Join[ExternalMomenta,LoopMomenta];
	SDim=L (L+1)/2+L (Length[ExternalMomenta]);  (* dimension for s_ij's *)
	
	ScalarVarRep=Join[Table[LoopMomenta[[i]] ExternalMomenta[[j]]->x[i,j],{i,1,Length[LoopMomenta]},{j,1,Length[ExternalMomenta]}]//Flatten,Table[LoopMomenta[[i]] LoopMomenta[[j]]->y[i,j],{i,1,Length[LoopMomenta]},{j,i,Length[LoopMomenta]}]//Flatten];
	
	ScalarVar=Join[Table[x[i,j],{i,1,Length[LoopMomenta]},{j,1,Length[ExternalMomenta]}]//Flatten,Table[y[i,j],{i,1,Length[LoopMomenta]},{j,i,Length[LoopMomenta]}]//Flatten];
	
	(* Propagator search *)
	
	If[Length[Propagators]<SDim,
		SGr=GroebnerBasis[Expand[Propagators]/.Kinematics/.ScalarVarRep,ScalarVar//Reverse,MonomialOrder->DegreeReverseLexicographic,CoefficientDomain->RationalFunctions];
		RSP=LT[#,ScalarVar//Reverse,DegreeReverseLexicographic]&/@SGr;
		RSP=Flatten[Intersection[Variables[#],ScalarVar]&/@RSP];
		ISP=Complement[ScalarVar,RSP];
		ScalarVarRepBack2=Join[Table[x[i,j]->(LoopMomenta[[i]]+ExternalMomenta[[j]])^2,{i,1,Length[LoopMomenta]},{j,1,Length[ExternalMomenta]}]//Flatten,Table[y[i,j]->(LoopMomenta[[i]]+LoopMomenta[[j]])^2,{i,1,Length[LoopMomenta]},{j,i,Length[LoopMomenta]}]//Flatten];
		ISP=ISP/.ScalarVarRepBack2;
		Print["more propagators added ... ",ISP];
		Propagators=Join[Propagators,ISP];
	];
	
	
	
	var=Table[z[i],{i,1,Length[Propagators]}];
	BList=Table[B[i],{i,1,Length[Propagators]}];
	AList=Table[A[i],{i,1,Length[Propagators]}];
	
	BaikovMatrix=Table[Momenta[[i]] Momenta[[j]]//Expand,{i,1,Length[Momenta]},{j,1,Length[Momenta]}]/.Kinematics/.ScalarVarRep;

	GramMatrix=BaikovMatrix[[1;;n-1,1;;n-1]];
	
	LoopExternalScalars=Table[x[i,j],{i,1,L},{j,1,n-1}];

	(* rowIndex2 must correspond to the loop component *) 
	ScalarTangentSet=Flatten[Table[ScalarTangentVector[i,j],{i,1,L+n-1},{j,n,L+n-1}],1];
	
	(* Extended tangent set, WITH cofactor*)
	ScalarExtendedTangentSet=Flatten[Table[If[i!=j,Append[ScalarTangentVector[i,j],0],Append[ScalarTangentVector[i,j],-2]],{i,1,L+n-1},{j,n,L+n-1}],1];
	
	
	
	
	BaikovKernelScalar=Det[BaikovMatrix];
	MatrixA=Coefficient[Expand[#]/.Kinematics/.ScalarVarRep,ScalarVar]&/@Propagators;
	Vectorb=(Expand[#]/.zeroLoopMomenta/.Kinematics)&/@Propagators;
	
	(* z' s = MatrixA.ScalarVar + Vectorb, conversion between s_ij (x_ij here) to Baikov z's *)
	
	BaikovRevRep=MapThread[#1->#2&,{var,MatrixA.ScalarVar+Vectorb}];
	BaikovRep=MapThread[#1->#2&,{ScalarVar,Inverse[MatrixA].(var-Vectorb)}];
	BaikovKernel=BaikovKernelScalar/.BaikovRep;
	
	Parameters=Select[Variables[BaikovMatrix/.BaikovRep],!Head[#]===z&];
	
	TangentSet=MatrixA.#&/@ScalarTangentSet/.BaikovRep//Factor;
	ExtendedTangentSet=Transpose[Join[Transpose[TangentSet],{Transpose[ScalarExtendedTangentSet]//Last}]]; (* including the cofactors *)
	
	
	ForwardRep[1]=Join[Table[z[i]->ToExpression["z"<>ToString[i]],{i,1,SDim}],Table[Parameters[[i]]->ToExpression["c"<>ToString[i]],{i,1,Parameters//Length}]];
	BackwardRep[1]=Reverse/@ForwardRep[1];
	ForwardRep[2]=Join[Table[ScalarVar[[i]]->ToExpression["z"<>ToString[i]],{i,1,SDim}],Table[Parameters[[i]]->ToExpression["c"<>ToString[i]],{i,1,Parameters//Length}]];
	BackwardRep[2]=Reverse/@ForwardRep[2];
	
	Scalar2sp=#[[2]]->(#[[1]]/.{x_ y_->sp[x,y],x_^2->sp[x,x]})&/@ScalarVarRep; 
	sp2Scalar=Join[Reverse/@Scalar2sp,
		Table[sp[ExternalMomenta[[i]],ExternalMomenta[[j]] ]->(ExternalMomenta[[i]]*ExternalMomenta[[j]]/.Kinematics),{i,1,Length[ExternalMomenta]},{j,1,Length[ExternalMomenta]}]//Flatten
		];
	
	PropagatorScalar=Expand[Propagators]/.ScalarVarRep/.Kinematics;
	
	(* Feynman representation *)
	
	Formula=(Propagators.var)/.(#->t #&/@LoopMomenta);
	MatrixA=D[SeriesCoefficient[Formula,{t,0,2}],{LoopMomenta},{LoopMomenta}]/2;	
	VectorB=Coefficient[SeriesCoefficient[Formula,{t,0,1}],LoopMomenta]/2;
	ScalarC=SeriesCoefficient[Formula,{t,0,0}];
	U=Det[MatrixA]//Factor;
	F=-U (Expand[ScalarC]/.Kinematics)+ Cancel[U (Expand[VectorB.Inverse[MatrixA].VectorB]/.Kinematics)];

	Print["L=",L," E=",Length[ExternalMomenta]];
	Print["Parameters ",Parameters];
	Print["Baikov variables: var=",var];
	Print["Physical quantities: \"ScalarVarRep,BaikovMatrix,BaikovRep,BaikovKernel\" are generated"];
	
];


(* Get two tangent modules for the problem *)


TangentModules[propIndex_,cutIndex_]:=Module[{M1,M2,M1ext,cut,ISPcondition},
		
		
		cut=z[#]->0&/@cutIndex;
		ISPcondition=#->1&/@(Complement[Global`var,z[#]&/@propIndex]);
		M1=Global`TangentSet/.cut;
		M1ext=Global`ExtendedTangentSet/.cut;
		M2=DiagonalMatrix[var]/.ISPcondition/.cut;
		Return[{M1,M1ext,M2}];

];


Print["Module Intersection: A Mathematica Interface for Generating IBPs with the Propagator Index Restricted."];
Print["Yang Zhang\n"];
Print["Please make sure that:"];
Print["1) Add all ISPs (irreducible scalar products) to  'Propagators'."];
Print["2) Only list linearly indepedent momenta in 'ExternalMomenta'."];
Print["3) Length[Propagators]==Length[LoopMomenta](Length[LoopMomenta]+1)/2 +Length[LoopMomenta]Length[ExternalMomenta] "];
Print["Disclaimer: this interface does not include the IBP reduction part. "];


(* ::Section:: *)
(*Cut*)


SingularIdeal[propIndex_,cutIndex_]:=Module[{cut,FF,SingularIdeal},
	 cut=z[#]->0&/@cutIndex;
	 FF=Global`BaikovKernel/.cut;
	 SingularIdeal=Join[D[FF,{Global`var}],{FF}];
	 Return[SingularIdeal];
];


(* Options[CutPrepare]:={TangentVector->True,NumericMode->False,Modulus->0}; *)


(* CutPrepare[cutIndex1_,OptionsPattern[]]:=Module[{cutIndex=cutIndex1//Sort,TestVectors,TCut,timer,rep={}},
	Global`cut=z[#]->0&/@cutIndex;
	Global`varcut=Complement[Global`var/.Global`cut,{0}];
	Print["remaining variables on the cut: ",varcut];
	Global`BaikovKernelCut=BaikovKernel/.cut;	
	Global`ObstructionMatrix=(TangentSet/.cut)[[All,cutIndex]];
	
	If[OptionValue[TangentVector],
		timer=AbsoluteTime[];
		If[OptionValue[NumericMode],rep=Global`Numerics];
		Print[rep];
		
		TestVectors=SingularSyz[ObstructionMatrix/.rep,varcut,MonomialOrder->DegreeReverseLexicographic,Modulus->OptionValue[Modulus],SingularOptions->{"redSB","redTail"}].(ExtendedTangentSet/.cut/.rep);
		
		TCut=TestVectors[[All,Complement[Range[SDim+1],cutIndex]]];
		Print["Tangent vector for the cut Baikov polynomial found ... ",AbsoluteTime[]-timer];
		Return[TCut];
	];
	
	
	
]; *)


(* ::Section:: *)
(*Singular Interface*)


ModuleSlash[m_]:=Table[If[Union[m[[j]]]==={0},Nothing,m[[j]]],{j,1,Length[m]}];


Vector2gen[vec_,mode_]:=vec.Table[gen[j],{j,1,Length[vec]}]/.ForwardRep[mode];
Vector2SingularForm[vec_,mode_]:=StringReplace[ToString[InputForm[Vector2gen[vec,mode]]],{"gen["~~Shortest[x__]~~"]":>"gen("<>x<>")","{"->"[","}"->"]"}];
Module2SingularForm[m_,mode_]:=StringReplace[ToString[Vector2SingularForm[#,mode]&/@m],{"{"->"","}"->""}];


SingularIntersectionText="LIB \"matrix.lib\";
ring R = (MODULUS), (VAR, PARAMETERS), (dp(LEN1),dp(LEN2));
option(prot);
degBound=DEGBOUND;
module m1 = MODULE1;
module m2 = MODULE2;
module m=m1+m2;
module m1ext = M1ext;
module a=syz(m);
matrix matrixa=a;
matrix matrixa1=submat(matrixa,1..M1SIZE,1..size(a));
matrix matrixm1ext=m1ext;
module m12old=module(matrixm1ext*matrixa1);
ring R2 = (MODULUS,PARAMETERS), (VAR), dp(LEN1);
module m12=imap(R,m12old);
module mInt=simplify(m12,SimplificationStrategy);
write(\"OUTPUTFILE\",string(mInt));
";


SingularSyzText="LIB \"matrix.lib\";
ring R = (MODULUS), (VAR, PARAMETERS), (dp(LEN1),dp(LEN2));
option(prot);
degBound=DEGBOUND;
module m1 = MODULE1;
module a=syz(m1);
ring R2 = (MODULUS,PARAMETERS), (VAR), dp(LEN1);
module a2=imap(R,a);
module a3=simplify(a2,SimplificationStrategy);
write(\"OUTPUTFILE\",string(a3));
";


Options[SingularIntersectionMaker]={Modulus->0,SimplificationRules->34,ScriptFile->TemporaryDirectory<>"intersection_test.sing",
OutputFile->TemporaryDirectory<>"intersection_result.txt",ScriptOnly->False,degBound->0
};


SingularIntersectionMaker[M1input_,M1extinput_,M2input_,varRedundant_,parameterRedundant_,OptionsPattern[]]:=Module[{M1,M1ext,M2,SingularScript,forwardRep,backwardRep,var,parameters,varpara,len1,len2,
varString,parameterString},
	If[FileExistsQ[OptionValue[OutputFile]],DeleteFile[OptionValue[OutputFile]]];
	M1=ModuleSlash[M1input];
	M1ext=ModuleSlash[M1extinput];
	M2=ModuleSlash[M2input];
	varpara=Variables[Join[M1,M2]];
	var=ListIntersect[varRedundant,varpara];  (* Delete the variables not in the modules *)
	parameters=ListIntersect[parameterRedundant,varpara];   (* Delete the parameters not in the modules *)
	If[parameters=={},parameters=parameterRedundant[[1]]];   (*  If there is no parameter, to fit in the Singular code, pick up one parameter *)
	
	varString=StringReplace[ToString[var/.ForwardRep[1]],{"{"->"","}"->""}];
	parameterString=StringReplace[ToString[parameters/.ForwardRep[1]],{"{"->"","}"->""}];
	
	SingularScript=StringReplace[SingularIntersectionText,{"VAR"->varString,"PARAMETERS"->parameterString}];
	SingularScript=StringReplace[SingularScript,{"MODULUS"->ToString[Modulus//OptionValue],"LEN1"->ToString[Length[var]],"LEN2"->ToString[Length[parameters]]}];
	SingularScript=StringReplace[SingularScript,{"MODULE1"->Module2SingularForm[M1,1],"M1ext"->Module2SingularForm[M1ext,1],"MODULE2"->Module2SingularForm[M2,1],"M1SIZE"->ToString[Length[M1]]}];
	SingularScript=StringReplace[SingularScript,"OUTPUTFILE"->OptionValue[OutputFile]];
	SingularScript=StringReplace[SingularScript,{"SimplificationStrategy"->ToString[OptionValue[SimplificationRules]],"DEGBOUND"->ToString[OptionValue[degBound]]}];
	Print[OptionValue[ScriptFile]];
	Export[OptionValue[ScriptFile],SingularScript,"Text"];
	If[OptionValue[ScriptOnly], Return[]];
];


Options[IntersectionRead]=Options[SingularIntersectionMaker];
IntersectionRead[OptionsPattern[]]:=Module[{dim,string,vectors},
	dim=Global`SDim+1;
	If[!FileExistsQ[OptionValue[OutputFile]],Return[]];
	string=StringReplace["{"<>Import[OptionValue[OutputFile]]<>"}","gen("~~Shortest[x__]~~")":>"gen["~~x~~"]"];
	vectors=ToExpression[string];
	vectors=vectors/.gen[index_]:>UnitVector[dim,index]/.BackwardRep[1];
	Return[vectors];
];


Options[SingularIntersection]={Modulus->0,SimplificationRules->34,ScriptFile->TemporaryDirectory<>"intersection_test.sing",
OutputFile->TemporaryDirectory<>"intersection_result.txt",ScriptOnly->False,degBound->0,VariableOrder->var,Cut->{}};
SingularIntersection[resIndex_,OptionsPattern[]]:=Module[{M1,M1ext,M2,SingularCommand,timer,vectors,cutIndex},
	cutIndex=OptionValue[Cut];
	If[!SubsetQ[resIndex,cutIndex],Print["Sorry... This version does not support the case with a cut propagator index UNrestricted ..."]; Return[];];
	{M1,M1ext,M2}=TangentModules[resIndex,cutIndex];
	varOrder=OptionValue[VariableOrder];
	SingularIntersectionMaker[M1,M1ext,M2,varOrder,Parameters,ScriptFile->OptionValue[ScriptFile],OutputFile->OptionValue[OutputFile],Modulus->OptionValue[Modulus],SimplificationRules->OptionValue[SimplificationRules],degBound->OptionValue[degBound]];
	SingularCommand=SingularDirectory<>" "<>OptionValue[ScriptFile];
	timer=AbsoluteTime[];
	Run[SingularCommand];
	Print["Singular running time ... ",AbsoluteTime[]-timer];
	vectors=IntersectionRead[OutputFile->OptionValue[OutputFile]];
	Return[vectors];
];


(* ::Section:: *)
(*IBP generator*)


Std[f_,ref_]:=Total[(G@@(ref-#[[1]]))*#[[2]]&/@CoefficientRules[f,var,DegreeReverseLexicographic]];


Options[IBPGenerator]:={Cut->{}};
IBPGenerator[vector_,RestrictedPropIndex_,OptionsPattern[]]:=Module[{i,b,ref,reflocal,term1,term2=0,h,f,bb,cutIndex},
	cutIndex=OptionValue[Cut];
	If[!SubsetQ[RestrictedPropIndex,cutIndex],Print["Sorry... This version does not support the case with a cut propagator index UNrestricted ..."]; Return[];];
	If[cutIndex!={},Return[IBPCutGenerator[vector,RestrictedPropIndex,cutIndex]];];
	b=vector[[-1]];
	h=L+(n-1)+1;
	ref=Table[m[i],{i,1,SDim}];
	term1=Std[-((d-h)/2)b,ref];
	For[i=1,i<=SDim,i++,
		If[MemberQ[RestrictedPropIndex,i],
			bb=Cancel[vector[[i]]/z[i]];
			f=(-m[i]+1)bb+z[i]*D[bb,z[i]];
			term2+=Std[f,ref];
			,
			reflocal=ref/.m[i]->m[i]+1;
			f=-m[i] vector[[i]]+z[i]*D[vector[[i]],z[i]];
			term2+=Std[f,reflocal];
		];
	];
	Return[term1+term2];
	
];


IBPCutGenerator[vector_,RestrictedPropIndex_,cutIndex_]:=Module[{i,b,ref,reflocal,term1,term2=0,h,f,bb,cutNormalization},
	b=vector[[-1]];
	h=L+(n-1)+1;
	cutNormalization=m[#]->1&/@cutIndex;
	(* Print[cutNormalization]; *)
	ref=Table[m[i],{i,1,SDim}]/.cutNormalization;
	term1=Std[-((d-h)/2)b,ref];
	For[i=1,i<=SDim,i++,
		If[MemberQ[cutIndex,i],Continue[];];
		If[MemberQ[RestrictedPropIndex,i],
			bb=Cancel[vector[[i]]/z[i]];
			f=(-m[i]+1)bb+z[i]*D[bb,z[i]];
			term2+=Std[f,ref];
			,
			reflocal=ref/.m[i]->m[i]+1;
			f=-m[i] vector[[i]]+z[i]*D[vector[[i]],z[i]];
			term2+=Std[f,reflocal];
		];
	];
	Return[term1+term2];

];


(* Test function *)
(* SectorSeed[sec_,propExtraDeg_,ISPDeg_]:=Module[{propIndex,ISPIndex,propbasis,ISPbasis,seeds},
	propIndex=Position[sec,1]//Flatten;
	ISPIndex=Position[sec,0]//Flatten//Reverse; (* !!! *)
	propbasis=(sec+Exponent[#,var])&/@monomialAnsatz[(z/@propIndex),propExtraDeg];
	ISPbasis=-Exponent[#,var]&/@monomialAnsatz[(z/@ISPIndex),ISPDeg];
	seeds=Flatten[Table[propbasis[[i]]+ISPbasis[[j]],{i,1,Length[propbasis]},{j,1,Length[ISPbasis]}],1];
	Return[seeds];
]; *)


(* ::Section:: *)
(*GKK Momentum Syzygy Approach*)


MomentumSyzygyMatrix[resIndices_,OptionsPattern[]]:=Module[{vAnsatzList,vlist,SyzOriginal,GKKvariables,GKKMatrix},
	vlist=Join[ExternalMomenta,LoopMomenta];
	vAnsatzList=Table[Sum[a[i,j] vlist[[j]],{j,1,L+n-1}],{i,1,L}];
	
	GKKvariables=Join[Flatten[Table[a[i,j],{i,1,L},{j,1,L+n-1}]],b/@resIndices];
	

	SyzOriginal=((Sum[vAnsatzList[[i]]*D[Propagators[[#]],LoopMomenta[[i]]],{i,1,L}]-b[#]Propagators[[#]]//Expand)/.ScalarVarRep/.Kinematics)&/@resIndices;
	GKKMatrix=CoefficientArrays[SyzOriginal,GKKvariables][[2]]//Normal;
	
	(**)
	
	Return[GKKMatrix];

];


Options[SingularSyzMaker]={Modulus->0,SimplificationRules->34,ScriptFile->TemporaryDirectory<>"syzygy_test.sing",
OutputFile->TemporaryDirectory<>"syz_result.txt",ScriptOnly->False,degBound->0
};


SingularSyzMaker[M1input_,varRedundant_,parameterRedundant_,OptionsPattern[]]:=Module[{M1,M1ext,M2,SingularScript,backwardRep,var,parameters,varpara,len1,len2,
varString,parameterString},
	If[FileExistsQ[OptionValue[OutputFile]],DeleteFile[OptionValue[OutputFile]]];
	(* M1=ModuleSlash[M1input]; *)
	M1=M1input;
	var=ListIntersect[varRedundant,M1//Variables];  (* Delete the variables not in the modules *)

	parameters=ListIntersect[parameterRedundant,M1//Variables];   (* Delete the parameters not in the modules *)
	If[parameters=={},parameters=parameterRedundant[[1]]];   (*  If there is no parameter, to fit in the Singular code, pick up one parameter *)
	
	varString=StringReplace[ToString[var/.ForwardRep[2]],{"{"->"","}"->""}];
	parameterString=StringReplace[ToString[parameters/.ForwardRep[2]],{"{"->"","}"->""}];
	
	SingularScript=StringReplace[SingularSyzText,{"VAR"->varString,"PARAMETERS"->parameterString}];
	SingularScript=StringReplace[SingularScript,{"MODULUS"->ToString[Modulus//OptionValue],"LEN1"->ToString[Length[var]],"LEN2"->ToString[Length[parameters]]}];
	SingularScript=StringReplace[SingularScript,{"MODULE1"->Module2SingularForm[M1,2],"M1SIZE"->ToString[Length[M1]]}];
	SingularScript=StringReplace[SingularScript,"OUTPUTFILE"->OptionValue[OutputFile]];
	SingularScript=StringReplace[SingularScript,{"SimplificationStrategy"->ToString[OptionValue[SimplificationRules]],"DEGBOUND"->ToString[OptionValue[degBound]]}];
	Print[OptionValue[ScriptFile]];
	
	Export[OptionValue[ScriptFile],SingularScript,"Text"];
	If[OptionValue[ScriptOnly], Return[]];
];


Options[SyzygyRead]={OutputFile->TemporaryDirectory<>"syz_result.txt"};
SyzygyRead[dim_,OptionsPattern[]]:=Module[{string,vectors},

	If[!FileExistsQ[OptionValue[OutputFile]],Return[]];
	string=StringReplace["{"<>Import[OptionValue[OutputFile]]<>"}","gen("~~Shortest[x__]~~")":>"gen["~~x~~"]"];
	vectors=ToExpression[string];
	vectors=vectors/.gen[index_]:>UnitVector[dim,index]/.BackwardRep[2];
	Return[vectors];   (* Must be transpose for the Singular output *)
];


Options[GKKSyzygy]={Modulus->0,SimplificationRules->34,ScriptFile->TemporaryDirectory<>"syz_test.sing",
OutputFile->TemporaryDirectory<>"syz_result.txt",ScriptOnly->False,degBound->0,VariableOrder->ScalarVar};
GKKSyzygy[resIndex_,OptionsPattern[]]:=Module[{M1,M1ext,M2,SingularCommand,timer,vectors,cutIndex,i,GKKvariables,vAnsatzList,vlist,result},

	M1=MomentumSyzygyMatrix[resIndex];
	varOrder=OptionValue[VariableOrder];
	(* Here GKK matrix must be transposed for the singular syz. *)
	SingularSyzMaker[M1//Transpose,varOrder,Parameters,ScriptFile->OptionValue[ScriptFile],OutputFile->OptionValue[OutputFile],Modulus->OptionValue[Modulus],SimplificationRules->OptionValue[SimplificationRules],degBound->OptionValue[degBound]];
	SingularCommand=SingularDirectory<>" "<>OptionValue[ScriptFile];
	timer=AbsoluteTime[];
	Run[SingularCommand];
	Print["Singular running time ... ",AbsoluteTime[]-timer];
	vlist=Join[ExternalMomenta,LoopMomenta];
	
	vectors=SyzygyRead[Dimensions[M1][[2]],OutputFile->OptionValue[OutputFile]];
	vAnsatzList=Table[Sum[a[i,j] vlist[[j]],{j,1,L+n-1}],{i,1,L}];
	GKKvariables=Join[Flatten[Table[a[i,j],{i,1,L},{j,1,L+n-1}]],b/@resIndex];

	result=Table[(vAnsatzList/.Table[GKKvariables[[j]]->vectors[[i,j]],{j,1,Length[GKKvariables]}]).(d/@LoopMomenta),{i,1,Length[vectors]}];
	
	Return[result];
];


(* The input can be a scalar function in x_ij variables or sp[v1,v2] variables *)
ScalarDiff[expr1_,momentum_]:=Module[{expr,derivative},
	expr=expr1/.Scalar2sp;
	If[!MemberQ[Join[LoopMomenta,ExternalMomenta],momentum],Return[0]];
	derivative=D[expr,momentum];
	derivative=derivative/.{\!\(\*SuperscriptBox[\(sp\), 
TagBox[
RowBox[{"(", 
RowBox[{"0", ",", "1"}], ")"}],
Derivative],
MultilineFunction->None]\)[x_,y_]:>v[x],\!\(\*SuperscriptBox[\(sp\), 
TagBox[
RowBox[{"(", 
RowBox[{"1", ",", "0"}], ")"}],
Derivative],
MultilineFunction->None]\)[x_,y_]:>v[y]};
	Return[derivative];

];


vectorProduct[v1_,v2_]:=Coefficient[v1/.v[x_]:>x,Join[ExternalMomenta,LoopMomenta]].BaikovMatrix.Coefficient[v2/.v[x_]:>x,Join[ExternalMomenta,LoopMomenta]];


Vector2IBP[GKKvector_,OptionsPattern[]]:=Module[{expr,derivative,result=0,dlist,Allvectors,GKKvectorList,i,j,Int,dint,coef},
	dlist=d/@LoopMomenta;
	Allvectors=Join[ExternalMomenta,LoopMomenta];
	GKKvectorList=Coefficient[GKKvector,dlist];
	
	(* int derivatives *)
	For[i=1,i<=Length[dlist],i++,
			dint=Sum[-m[j]*ScalarDiff[PropagatorScalar[[j]]/z[j],LoopMomenta[[i]]],{j,1,Length[Propagators]}];
			result+=vectorProduct[GKKvectorList[[i]],dint];
	];
	
	(* coefficient derivatives *)
	For[i=1,i<=Length[dlist],i++,
			For[j=1,j<=Length[Allvectors],j++,
					coef=Coefficient[GKKvectorList[[i]],Allvectors[[j]]];
					result+=vectorProduct[ScalarDiff[coef,LoopMomenta[[i]]],Allvectors[[j]]];
			];
	];
	(* vector derivatives *)
	For[i=1,i<=Length[dlist],i++,
			
					result+=Coefficient[GKKvectorList[[i]],LoopMomenta[[i]]]*d;
			
	];
	result=result/.sp2Scalar/.BaikovRep;
	(* Print[result]; *)
	shift=(Times@@var)^100;   (* To make the result as a polynomial in Baikov variables *)
	ref=m[#]+100&/@Range[Length[PropagatorScalar]];
	
	result=Std[result*shift//Together,ref];
	Return[result];
];


(* ::Section:: *)
(*IBP sector Tools*)


Sector[int_]:=Table[If[int[[j]]>0,1,0],{j,1,Length[int]}]
SectorHeight[int_]:=Count[int/.G->List,u_/;u>0];

SectorIndex[int_]:=Table[If[int[[j]]>0,j,Nothing],{j,1,Length[int]}] (* Azurite form *)



ISPDegree[int_]:=-Total[Select[int/.G->List,#<0&]];
PropagatorDegree[int_]:=Sum[If[int[[j]]>1,int[[j]]-1,0],{j,1,Length[int]}]; 

(* IntegralAbsDegree[int_]:=Abs/@((int/.G->List)-Sector[int]); *)
ISPPower[int_]:=Table[If[int[[j]]<0,int[[j]],0],{j,1,Length[int]}] 

IntegralAbsDegree[int_]:=PropagatorDegree[int]+ISPDegree[int];

IntegralOrdering[int_]:=Join[{SectorHeight[int],Sector[int],PropagatorDegree[int]+ISPDegree[int],PropagatorDegree[int],ISPPower[int]}]  (* KEY FUNCTION *)


IntegralList[IBP_]:=SortBy[Select[Variables[IBP],Head[#]==G&],IntegralOrdering]//Reverse;

LeadingIntegral[IBP_]:=Block[{list},
	list=IntegralList[IBP];
	If[list!={},Return[list[[1]]],Return[{}]];
];



IBPSectorHeight[IBP_]:=Block[{LT},LT=LeadingIntegral[IBP]; If[LT==={},Return[-1],Return[SectorHeight[Int]]];];
IBPWeight[IBP_]:=Max[Total[IntegralAbsDegree[#]]&/@IntegralList[IBP]];

(* IBPISPDegree[f_]:=Max[ISPDegree/@Variables[f]]; *)







IntegerPartition[n_,len_]:=Module[{cc,clist,conditions},
	clist=Table[cc[i],{i,1,len}];
	conditions=Table[cc[i]>=0,{i,1,len}];
	
	AppendTo[conditions,Sum[cc[i],{i,1,len}]<=n];
	ss=Solve[conditions,clist,Integers];
	Return[clist/.ss];
];
positionArrange[positions_,values_,len_]:=SparseArray[MapThread[#1->#2&,{positions,values}],len]//Normal;


IntegralRealization[FundamentalIBP_,degree_]:=FundamentalIBP/.Table[m[i]->degree[[i]],{i,1,SDim}];


SectorSeeding[FundamentalIBPList_,sector_,r_,s_]:=Module[{dshifts,denshifts,numshifts,secheight,secindex,seeds,IBPs},
		secheight=SectorHeight[sector];
		secindex=SectorIndex[sector];
		
		denshifts=positionArrange[secindex,#,SDim]&/@IntegerPartition[r,secheight];
		numshifts=-positionArrange[Complement[Range[SDim],secindex],#,SDim]&/@IntegerPartition[s,SDim-secheight];
		
		
		
		seeds=Flatten[Table[sector+denshifts[[i]]+numshifts[[j]],{i,1,Length[denshifts]},{j,1,Length[numshifts]}],1];
		IBPs=Flatten[Table[IntegralRealization[FundamentalIBPList[[i]],seeds[[j]]],{i,1,Length[FundamentalIBPList]},{j,1,Length[seeds]}]];
	
		Print["sector:",sector,", denominator increase bound ",r,", numerator increase bound ",s,":   ",Length[IBPs]," IBPs generated..."];
			Return[IBPs];
	];


pivots[matrix_]:=Module[{ARLonglist},
	ARLonglist=GatherBy[matrix//ArrayRules,#[[1,1]]&];
	Return[#[[1,1,2]]&/@(ARLonglist[[;;-2]])];
];


Options[IndepedentIBP]:={Modulus->42013,SortFunction->ByteCount,SectorIndependence->Null};
IndepedentIBP[IBPs1_,Numerics_,OptionsPattern[]]:=Module[{IBPs,ttt,Integrals,nM,RM,RIBP,i,additionalIntegrals,reducedIntegrals,index,nMRREF},
	ttt=AbsoluteTime[];
	IBPs=SortBy[IBPs1,OptionValue[SortFunction]];
	Integrals=IntegralList[IBPs];
	If[ListQ[OptionValue[SectorIndependence]],Integrals=Select[Integrals,Sector[#]==OptionValue[SectorIndependence]&]];
	
	nM=CoefficientArrays[IBPs/.Numerics/.d->1/137,Integrals][[2]];
	nMRREF=RowReduce[Transpose[nM],Modulus->OptionValue[Modulus]];
	index=pivots[nMRREF];
	IBPs=IBPs[[index]];
	Integrals=IntegralList[IBPs];
	If[ListQ[OptionValue[SectorIndependence]],
					Integrals=Select[Integrals,Sector[#]==OptionValue[SectorIndependence]&];
					Print[Length[index]," independent IBPs, ",Length[Integrals]," integrals on this sector"];
						,
						
					
					Print[Length[index]," independent IBPs, ",Length[Integrals]," integrals"];
					];

	
	
	Return[IBPs];
	

]


Options[SectorStatus]:={Modulus->42013,Gap->10};
SectorStatus[sector_,IBPs_,Numerics_,OptionsPattern[]]:=Module[{ttt,Integrals,nM,RM,RIBP,i,additionalIntegrals,reducedIntegrals,index,nMRREF,irreducibleIndex,MIindex={},MIList={},reducedIndex={},position},
	ttt=AbsoluteTime[];

	
	Integrals=Select[IntegralList[IBPs],Sector[#]==sector&];  (* Drop the tails  *)
	
	nM=CoefficientArrays[IBPs/.Numerics/.d->1/137,Integrals][[2]];
	nMRREF=RowReduce[nM,Modulus->OptionValue[Modulus]];
	index=pivots[nMRREF];
	
	irreducibleIndex=Complement[Range[Dimensions[nM][[2]]],index];
	
	If[irreducibleIndex=={}||Dimensions[nM][[2]]-irreducibleIndex[[-1]]>OptionValue[Gap],Print["No master integral exists in the sector ", sector]; Return[{}]];
	
	
	AppendTo[MIindex,irreducibleIndex[[-1]]];
	For[i=Length[irreducibleIndex]-1,i>=1,i--,
		If[irreducibleIndex[[i+1]]-irreducibleIndex[[i]]>OptionValue[Gap],Break[]];
		AppendTo[MIindex,irreducibleIndex[[i]]];
	];
	MIList=Integrals[[MIindex]]//IntegralList;
	
	For[i=1,i<=Length[nM],i++,
		position=Complement[#[[1,1]]&/@ArrayRules[nMRREF[[i]]],Join[MIindex,{Blank[]}]];
		
		If[Length[position]==1,AppendTo[reducedIndex,position[[1]]]];
	];
	Global`SectorReducedIntegrals=Join[Integrals[[reducedIndex]],MIList];
	Print[MIList//Length," master integral(s) in the sector ", sector,": ",MIList];
	Print["Reduced integrals are temporarily stored in 'SectorReducedIntegrals'"];
	
	
	
]
