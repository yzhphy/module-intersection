(* ::Package:: *)

TemporaryDirectory = $HomeDirectory <> "/exchange/"
SingularDirectory = "/usr/local/bin/Singular"





Get["/Users/Beryllium/packages/ModInt/math/ModuleIntersection.wl"]


NotebookDirectory[]


LoopMomenta = {l1};
ExternalMomenta = {k1, k2, k4};
Propagators = {l1^2, (l1 - k1)^2, (l1 - k1 - k2)^2, (k4 + l1)^2};
Kinematics = {k1^2 -> 0, k2^2 -> 0, k4^2 -> 0, k1 k2 -> s/2,
   k2 k4 -> (-s - t)/2, k1 k4 -> t/2};
Parameters = {s, t};
Numerics = {s -> 1, t -> -3};
Prepare[]


varOrder=var//Reverse


?SingularIntersection


vectors1=SingularIntersection[{1,2,3,4},{1,3},varOrder,Parameters]


IBPGenerator[vectors1[[1]],{1,2,3,4}]/.{m[1]->1,m[2]->1,m[3]->1,m[4]->1}
