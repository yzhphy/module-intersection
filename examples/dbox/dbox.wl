(* ::Package:: *)

TemporaryDirectory = $HomeDirectory <> "/exchange/"
SingularDirectory = "/usr/local/bin/Singular"





Get["~/packages/ModInt/math/ModuleIntersection.wl"]


LoopMomenta = {l1, l2};
ExternalMomenta = {k1, k2, k4};
Propagators = {l1^2, (l1 - k1)^2, (l1 - k1 - k2)^2, (l2 + k1 +
      k2)^2, (l2 - k4)^2, l2^2, (l1 + l2)^2, (k4 + l1)^2, (k1 + l2)^2};
Kinematics = {k1^2 -> 0, k2^2 -> 0, k4^2 -> 0, k1 k2 -> s/2,
   k2 k4 -> (-s - t)/2, k1 k4 -> t/2};
Prepare[]


(* ::Section:: *)
(*Restricted propagator indices {1,2,3,4,5,6,7}*)


resIndices={1,2,3,4,5,6,7};


vectorsList1=SingularIntersection[resIndices];


vectorsList1//Length


IBPGenerator[vectorsList1[[2]],resIndices]//CollectG


(* ::Section:: *)
(*Restricted propagator indices {1,2,5,6,7}*)


resIndices={1,2,5,6,7};
vectorsList2=SingularIntersection[resIndices];


IBPGenerator[vectorsList2[[1]],resIndices]//CollectG


(* ::Section:: *)
(*Restricted propagator indices {1,2,3,4,5,6,7} with cut {1,2,3,4,5,6,7}*)


resIndices={1,2,3,4,5,6,7};
cutIndices={1,2,3,4,5,6,7};


vectorsList3=SingularIntersection[resIndices,Cut->cutIndices];


IBPGenerator[vectorsList3[[1]],resIndices,Cut->cutIndices]//CollectG


(* ::Section:: *)
(*Restricted propagator indices {1,2,3,4,5,6,7} with cut {2,5,7}*)


resIndices={1,2,3,4,5,6,7};
cutIndices={2,5,7};


vectorsList4=SingularIntersection[resIndices,Cut->cutIndices];
vectorsList4//Length


IBPGenerator[vectorsList4[[2]],resIndices,Cut->cutIndices]//CollectG


(* ::Section:: *)
(*GKK approach with restricted propagator indices {1,2,3,4,5,6,7}*)


syz1=GKKSyzygy[{2,5,7}];





syz1[[2]]
